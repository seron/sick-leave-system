package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Mikael Ekroth.
 * Date: 2014-03-16
 * Time: 21:56
 * Description:
 *
 * Employees absence information
 */
@Entity
public class Absence extends Model {

	/** Primary key. */
	@Id
	private long id;
	/** Connects the relationship between Absence and User. */
	@ManyToOne
	private User user;

	/** Timestamp when the absence added.*/
	private Date added;
	/** Absence by is sick. */
	private  boolean isSick;

	/** Absence by child is sick. */
	private  boolean childSickness;
	/** The childs registration number. */
	private  String childRegistrationNum;
	/** Absence by medical certification from doctor. */
	private  boolean medicalCert;
	/** Medical cert started. */
	private  Date medicalStart;
	/** Medical cert ends. */
	private  Date medicalEnd;

	/**
	 * Creation of a new absence object focus on sickness.
	 * @param sick if sick true/false
	 */
	public Absence(final boolean sick) {
		isSick = sick;
		childRegistrationNum = null;
		childSickness = false;
		medicalCert = false;
		medicalStart = null;
		medicalEnd = null;
	}

	/**
	 * Creation of a new absence object focus on child sickness.
	 * @param registrationNumber the child registration number
	 * @param isChildSick sets child is sick true/false
	 */
	public Absence(final String registrationNumber,
				   final boolean isChildSick) {
		childRegistrationNum = registrationNumber;
		childSickness = isChildSick;
		isSick = false;
		medicalCert = false;
		medicalStart = null;
		medicalEnd = null;
	}

	/**
	 * Creation of a new absence object
	 * focus on absence by medical certification.
	 * @param isCert is absence by medical cert true/false
	 * @param certStarts start of the medical cert
	 * @param certEnds end of the medical cert
	 */
	public Absence(final boolean isCert,
			final Date certEnds,
			final Date certStarts) {
		medicalEnd = certEnds;
		medicalCert = isCert;
		medicalStart = certStarts;
		isSick = false;
		childRegistrationNum = null;
		childSickness = false;
	}

	/**
	 * The EBean ORM finder method for database queries.
	 * @return the finder method for Absence
	 */
	public static Finder<Long, Absence> find() {
		return new Finder<>(Long.class, Absence.class);
	}

	/**
	 * @return absence id
	 */
	public final long getId() {
		return id;
	}

	/**
	 * @param newId sets absence id
	 */
	public final void setId(final long newId) {
		id = newId;
	}

	/**
	 * @return the user
	 */
	public final User getUser() {
		return user;
	}

	/**
	 * @param newUser the user to set
	 */
	public final void setUser(final User newUser) {
		user = newUser;
	}

	/**
	 * @return the date when it's added
	 */
	public final Date getAdded() {
		return added;
	}

	/**
	 * @param timeStamp Set's new added date
	 */
	public final void setAdded(final Date timeStamp) {
		added = timeStamp;
	}

	/**
	 * @return True if employee is sick
	 */
	public final boolean isSick() {
		return isSick;
	}

	/**
	 * @param sick Sets if employee is sick
	 */
	public final void setSick(final boolean sick) {
		isSick = sick;
	}

	/**
	 * @return True/False if the employee is home with sick child
	 */
	public final boolean isChildSickness() {
		return childSickness;
	}

	/**
	 * @return The child registration number
	 */
	public final String getChildRegistrationNum() {
		return childRegistrationNum;
	}

	/**
	 * @return True if employee is absence by medical certification
	 */
	public final boolean isMedicalCert() {
		return medicalCert;
	}

	/**
	 * @return The time span the medical certification have
	 */
	public final String getMedicalCertTime() {
		SimpleDateFormat dateFormat =
				new SimpleDateFormat("yyyy-MM-dd");
		return "Fr.o.m: " + dateFormat.format(medicalStart)
				+ " T.o.m: " + dateFormat.format(medicalEnd);
	}

	/**
	 * @return The added date
	 */
	public final String getRegisteredAbsence() {
		SimpleDateFormat dateFormat =
				new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return dateFormat.format(added);
	}

	/**
	 * @param isChildSick Sets if absence is by child sickness
	 */
	public final void setChildSickness(final boolean isChildSick) {
		childSickness = isChildSick;
	}

	/**
	 * @param registrationNum Sets the childs registration number
	 */
	public final void setChildRegistrationNum(
			final String registrationNum) {
		childRegistrationNum = registrationNum;
	}

	/**
	 * @param isMedicalCert Sets if absence is by medical cert
	 */
	public final void setMedicalCert(final boolean isMedicalCert) {
		medicalCert = isMedicalCert;
	}

	/**
	 * @param start Sets the start of medical cert
	 */
	public final void setMedicalStart(final Date start) {
		medicalStart = start;
	}

	/**
	 * @param end Sets the end of medical cert
	 */
	public final void setMedicalEnd(final Date end) {
		medicalEnd = end;
	}
}
