package models;

import play.db.ebean.Model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple representation of a user.
 * Created by Ivan Videsvall on 2014-03-14.
 *
 * Modified by Mikael Ekroth 2014-03-16
 */
@Entity
public class User extends Model {

	@Id
	private long id;

	private String emplyeeNumber;

	/** The user's name and password.*/
	private String username;

	/**
	 * User first name
	 * Added by Mikael Ekroth
	 * */
	private String firstName;

	/**
	 * User last name
	 * Added by Mikael Ekroth
	 * */
	private String lastName;

	/** The user's password. */
	private String password;

    /**
     * The if the user is admin
     * Added by Mikael Ekroth */
    private boolean admin;

	/**
	 * Has connection to many absence
	 * Added by Mikael Ekroth */
	@OneToMany(mappedBy = "user", cascade= CascadeType.PERSIST)
 	private List<Absence> absences = new ArrayList<>();

	/**
	 * Creates a new User instance
	 *
	 * @param username the name
	 * @param password the password
	 */
	public User(String username, String password) {
		this.username = username;
		this.password = password;
	}

	/**
	 * Creates a new User instance.
	 *
	 * Added by Mikael Ekroth
	 * @param username The name.
	 * @param password The password.
	 * @param firstName The user's fist name
	 * @param lastName The user's last nam
	 * @param emplyeeNumber The user's employeeNumber
	 */
	public User(String username, String password, String emplyeeNumber, String firstName, String lastName) {
		this(username, password);
		this.emplyeeNumber = emplyeeNumber;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	/**
	 * Returns the user's username.
	 * @return the name
	 */
	public String getUsername() {

		return username;
	}

	/**
	 * Returns the user's password.
	 * @return the password
	 */
	public String getPassword() {

		return password;
	}

	/********************************************************************
	 * Methods belove is added by Mikael Ekorth
	 ********************************************************************/

	/**
	 * The EBean ORM finder method for database queries
	 * @return the finder method for Absence
	 */
	public static Finder<Long, User> find() {
		return new Finder<>(Long.class, User.class);
	}

	/**
	 * Adds abscense to the specific user
	 * @param newAbsence user's new absence
	 */
	public void addAbsence(Absence newAbsence){
		if(newAbsence != null){
			return;
		}
		this.absences.add(newAbsence);
	}

   /**
	* GETTERS AND SETTERS
	********************************************************************/

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id sets the id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the users employeeNumber
	 * @return String - employee number
	 */
	public String getEmplyeeNumber() {
		return emplyeeNumber;
	}

	/**
	 * Sets the users employee number
	 * @param emplyeeNumber new employee number
	 */
	public void setEmplyeeNumber(String emplyeeNumber) {
		this.emplyeeNumber = emplyeeNumber;
	}

	/**
	 * Checks if the User is a admin
	 * @return true / false
	 */
	public boolean isAdmin() {
		return admin;
	}

	/**
	 * Sets the user to admin
	 * @param isAdmin sets if the user is admin
	 */
	public void setAdmin(boolean isAdmin) {
		this.admin = isAdmin;
	}

	/**
	 * Gets the user's last name
	 * @return String - user's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the last name of the user
	 * @param newLastName user's new last name
	 */
	public void setLastName(String newLastName) {
		this.lastName = newLastName;
	}

	/**
	 * Gets the user's first name
	 * @return String - user's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the first name of the user
	 * @param newFirstName user's new first name
	 */
	public void setFirstName(String newFirstName) {
		this.firstName = newFirstName;
	}

	/**
	 * Gets the full name of the user
	 * @return String - first name, last name
	 */
	public String getFullname(){
		return this.firstName + " "	+ this.lastName;
	}

	/**
	 * @return the absence attached to the user
	 */
	public List<Absence> getAbsences() {
		return absences;
	}

	/**
	 * @param absences sets list of absences to the user
	 */
	public void setAbsences(List<Absence> absences) {
		this.absences = absences;
	}
}
