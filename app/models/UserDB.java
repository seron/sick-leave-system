package models;

/**
 * Provides an in-memory repository for User.
 * Created by Ivan Videsvall on 2014-03-13.
 *
 * Modified by Mikael Ekroth on 2014-03-16.
 */
public final class UserDB {

	/**
	 * Private constructor to appease CheckStyle.
	 */
	private UserDB() { }

	/**
	 * Adds the specified user to the UserDB.
	 * @param username Their username.
	 * @param password Their password.
	 */
	public static void addUserInfo(String username, String password,
								   String empNumber, String firstName,
								   String lastName) {
		if(!isUser(username)){
			User newUser = new User(username, password, empNumber, firstName, lastName);
			newUser.save();
		} else {
			throw new RuntimeException("Username "+ username +" already exists");
		}

	}

	/**
	 * Returns true if the username represents a known user.
	 * @param username The username.
	 * @return True if known user, otherwise false.
	 */
	public static boolean isUser(String username) {
		return (User.find().where().eq("username", username).findUnique() != null);
	}

	/**
	 * Returns the User associated with the username, or null if not found.
	 * @param username The username.
	 * @return The UserInfo.
	 */
	public static User getUser(String username) {
		return User.find().where().eq("username", username).findUnique();
	}

	public static User getUserByEmployeeNumber(String empNumber){
		return User.find().where().eq("employeeNumber", empNumber).findUnique();
	}

	/**
	 * Returns true if username and password are valid credentials.
	 * @param username The username.
	 * @param password The password.
	 * @return True if username is a valid username and password is valid for
	 * that username.
	 */
	public static boolean isValid(String username, String password) {

		return ((username != null)
				&&
				(password != null)
				&&
				isUser(username)
				&&
				getUser(username).getPassword().equals(password));
	}
}
