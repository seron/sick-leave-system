package models;

import log.LogTag;
import play.Logger;
import play.db.ebean.Transactional;
import views.formdata.AbsenceFormData;
import views.formdata.ChildAbsenceFormData;
import views.formdata.MedicalCertFormData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static play.mvc.Controller.flash;

/**
 * Created by Mikael Ekroth.
 * Date: 2014-03-16
 * Time: 23:10
 * Description:
 */
public final class AbsenceDB {

	/** Activating the logging. */
	private static final Logger.ALogger LOG = Logger.of("application");

	/**
	 * Default constructor.
	 */
	private AbsenceDB() { }

	/**
	 * Adds a employees absence to the database.
	 * Auther Mikael Ekroth
	 * @param username the user
	 * @param formData the validated inputs from the form
	 */
	public static void addAbsence(final String username,
				final AbsenceFormData formData) {
		Absence newAbsence = new Absence(formData.isSick);
		saveAbcence(username, newAbsence);
	}

	/**
	 * Adds a child sickness absence to the database.
	 * Auther Mikael Ekroth
	 * @param username the user
	 * @param formData the validated inputs from the form
	 */
	public static void addChildAbsence(final String username,
				final ChildAbsenceFormData formData) {
		Absence newAbsence =
				new Absence(formData.registrationNumber, true);
		saveAbcence(username, newAbsence);
	}

	/**
	 * Adds a medical certification absence to the database.
	 * Auther Mikael Ekroth
	 * @param username the user
	 * @param formData the validated inputs from the form
	 */
	public static void addMedicalCert(final String username,
					final MedicalCertFormData formData) {

		try {
			Date start = convertToDate(formData.start);
			Date end = convertToDate(formData.end);
			Absence newAbsence = new Absence(true, end, start);
			saveAbcence(username, newAbsence);
		} catch (Exception e) {
			throw new RuntimeException(
					"Date are not correct formated."
			);
		}
	}

	/**
	 * Find all user's abscense.
	 * @param username the username
	 * @return list of all absence for the user
	 */
	public static List<Absence> findAllByUsername(
			final String username) {
		User currentUser = getUser(username);

		if (currentUser == null) {
			return null;
		} else {
			return currentUser.getAbsences();
		}
	}

	/**
	 * Gets the lasted added absence of an employee.
	 *
	 * @param empNumber the employee number
	 * @return last added absence
	 */
	public static Absence findAbsenceEmployee(final String empNumber) {
		User currentUser = getUserByEmployeeNum(empNumber);

		if (currentUser == null) {
			flash("error", "Anställd med det sökta anställnings nummer finns inte.");
			LOG.error(String.format("%s User with employee number %s doesn't exists.",
					LogTag.ERROR, empNumber));
			return null;
		} else {
			List<Absence> absences = currentUser.getAbsences();
			if (absences.size() > 0) {
				return absences.get(absences.size() - 1);
			} else {
				flash("error", "Anställd är inte frånvarande.");
				LOG.info(String.format("%s User with employee number %s is not absence.",
						LogTag.INFO, empNumber));
				return null;
			}
		}
	}

	/**
	 * Gets the user by username.
	 * @param username The user to search after
	 * @return Asked user.
	 */
	private static User getUser(final String username) {
		return User.find()
					.where()
					.eq("username", username)
					.findUnique();
	}

	/**
	 * Gets the user by the employee number.
	 * @param employeeNum The employee number to look after.
	 * @return Asked user.
	 */
	private static User getUserByEmployeeNum(final String employeeNum) {
		return User.find()
					.where()
					.eq("emplyeeNumber", employeeNum)
					.findUnique();
	}

	/**
	 * Converts string to date-object.
 	 * @param strDateToConvert The string to convert to date
	 * @return The converted string to date
	 * @throws ParseException Trows when the date are not correct
	 */
	private static Date convertToDate(
			final String strDateToConvert) throws ParseException {
		SimpleDateFormat dateFormat =
				new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.parse(strDateToConvert);
	}

	/**
	 * Saves the absence to the database
	 *
	 * Transactional annotation means that it
	 * start and stops an transaction every.
	 * time it will add a new absence.
	 *
	 * @param username the user it connects to
	 * @param newAbsence the absence to store
	 */
	@Transactional
	public static void saveAbcence(final String username,
						final Absence newAbsence) {
		User user = User.find()
						.where()
						.eq("username", username)
						.findUnique();

		newAbsence.setAdded(new Date());

		if (user == null) {
			throw new RuntimeException(
					"Could not find user" + user);
		}

		//Connects the absence to the user
		user.addAbsence(newAbsence);

		//Connects the user to this absence
		newAbsence.setUser(user);

		newAbsence.save();
		user.save();
	}
}
