package views.formdata;

/**
 * Created by Mikael Ekroth.
 * Date: 2014-03-13
 * Time: 15:08
 * Description:
 *
 * Holds the absence form data
 */
public class AbsenceFormData {

    /** Is is sick or not the absence. */
    public boolean isSick;

	/** Child sickness form data. */
	public String registrationNumber;

	/**
	 * Medical forms start data.
	 */
	public String start;

	/**
	 * Medical forms ends data.
	 */
	public String end;
}
