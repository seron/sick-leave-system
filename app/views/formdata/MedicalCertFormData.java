package views.formdata;

import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mikael Ekroth.
 * Date: 2014-03-13
 * Time: 15:56
 * Description:
 *
 * Data class which holds the form data from the
 * registration of a Medical certification.
 */
public class MedicalCertFormData {

	/** Medical cert start date. */
	public String start;

	/** Medical cert end date. */
	public String end;

	/**
	 * Default Constructor.
	 */
	public MedicalCertFormData() { }

	/**
	 * Validates the inputs from the user.
	 * Error occurs when any of the feilds is empty and if
	 * the user doesn't type a correct formatted date.
	 *
	 * Auther Mikael Ekorth
	 * @return list of the errors which occurred.
	 */
	public final List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<>();

		if (ValidateHelper.isEmptyField(start)
				|| !ValidateHelper.isDate(start)) {
			errors.add(new ValidationError("start",
					"Ange ett korrekt start datum."));
		}

		if (ValidateHelper.isEmptyField(end)
				|| !ValidateHelper.isDate(end)) {
			errors.add(new ValidationError("end",
					"Ange ett korrekt slut datum."));
		}
		return ValidateHelper.returnErrors(errors);
	}
	@Override
	public final String toString() {
		return "start: " + start + " | end: "+ end;
	}
}
