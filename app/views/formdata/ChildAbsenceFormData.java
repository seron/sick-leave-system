package views.formdata;

import play.data.validation.ValidationError;
import java.util.List;
import java.util.ArrayList;

/**
 * Created by Mikael Ekroth.
 * Date: 2014-03-13
 * Time: 15:42
 * Description:
 * Handles the child forms data.
 */
public class ChildAbsenceFormData {

	/** Childs registration number. */
	public String registrationNumber;

	/**
	 * Default constructor.
	 * Auther Mikael Ekroth
	 */
	public ChildAbsenceFormData() { }

	/**
	 * Validates the incoming form data.
	 * Returns a list of validation errors.
	 *
	 * Auther Mikael Ekroth
	 * @return list of validation errors.
	 */
	public final List<ValidationError> validate() {
		List<ValidationError> errors = new ArrayList<>();

		if (!ValidateHelper.isRegistrationNumber(registrationNumber)
		|| ValidateHelper.isEmptyField(registrationNumber)) {
			errors.add(new ValidationError("registrationNumber",
					"Ange ett korrekt personnummer."));
		}

		return ValidateHelper.returnErrors(errors);
	}

	@Override
	public final String toString() {
		return "registration number: " + registrationNumber;
	}
}
