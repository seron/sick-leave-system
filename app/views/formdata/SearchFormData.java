package views.formdata;

import play.data.validation.ValidationError;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ivan Videsvall on 2014-03-12.
 */
public class SearchFormData {

	/** Debug flag. */
	private static final Boolean DEBUG = true;
	/** The employee number. */
	public String employeeNr = "";

	/**
	 * Constructs a form data object with default values.
	 * Author - Ivan Videsvall
	 */
	public SearchFormData() { }

	/**
	 * Checks that form fields are valid. Called by bindFromRequest();
	 * Author - Ivan Videsvall
	 * @return a list of ValidationError if errors are found or null if
	 * no errors are found
	 */
	public List<ValidationError> validate() {

		List<ValidationError> errors = new ArrayList<>();

		if (ValidateHelper.isEmptyField(employeeNr)) {

			errors.add(
					new ValidationError("employeeNr",
							"Anställningsnummer krävs."));
		}

		if (!employeeNr.matches("\\d{3}")) {

			errors.add(
					new ValidationError("employeeNr",
							"Anställningsnummer måste bestå av tre siffror."));
		}

		return ValidateHelper.returnErrors(errors);
	}

	/**
	 * Returns the form data as a String.
	 * Author - Ivan Videsvall
	 * @return the form data as a String.
	 */
	@Override
	public String toString() {

		return employeeNr;
	}
}
