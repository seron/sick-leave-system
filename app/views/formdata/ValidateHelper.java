package views.formdata;

import play.data.validation.ValidationError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
/**
 * Created by Mikael Ekroth
 * Date: 2014-03-16
 * Time: 11:54
 * Description:
 *
 * Validation helper which validates similar form data.
 */
public class ValidateHelper {

	/**
	 * Checks if the field is null or empty.
	 *
	 * Auther Mikael Ekroth
	 * @param inputData The input from the user
	 * @return true - if the feild is empty
	 */
	public static boolean isEmptyField(String inputData){
		return inputData == null || inputData.length() == 0;
	}

	/**
	 * Returns the list of errors.
	 * @param errors list of validation errors
	 * @return null if the list is empty, else it returns the list of errors.
	 */
	public static List<ValidationError> returnErrors(List<ValidationError> errors){
		return errors.isEmpty() ? null : errors;
	}

	/**
	 * Returns true if the input data matches YYYY-MM-DD format date.
	 * @param inputData The input from the user
	 * @return true if it matches YYYY-MM-DD
	 */
	public static boolean isDate(String inputData){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			format.parse(inputData);
			return true;
		} catch (ParseException pe){
			return false;
		}
	}

	/**
	 * Returns true if the input data matches swedish registration number
	 * XXXXXX-YYYY format date.
	 * @param inputData The input from the user
	 * @return true if it matches XXXXXX-YYYY
	 */
	public static boolean isRegistrationNumber(String inputData){
		System.out.println(inputData.trim().matches("[0-9]{6}-[0-9]{4}"));
		return inputData.trim().matches("[0-9]{6}-[0-9]{4}");
	}
}
