package controllers;

import log.LogTag;
import models.Absence;
import models.AbsenceDB;
import models.User;
import models.UserDB;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.formdata.LoginFormData;
import views.formdata.SearchFormData;
import views.html.ErrorPage;
import views.html.Login;
import views.html.SearchEmployee;
import java.net.UnknownHostException;

/**
 * Implements the controllers for this application.
 *
 * @author - Ivan Videsvall
 */
public class Application extends Controller {

	/** Activating the logging. */
	private static final Logger.ALogger LOG = Logger.of("application");

	/** Debug flag. */
	private static final Boolean DEBUG = true;
	/** A reusable Search form. */
	private static Form<SearchFormData> searchForm =
								Form.form(SearchFormData.class);
	/** A reusable Login form. */
	private static Form<LoginFormData> loginForm =
								Form.form(LoginFormData.class);

	/**
	 * Returns the Search employee page.
	 *
	 * @author - Ivan Videsvall
	 * @return The Search employee page
	 */
	@Security.Authenticated(Secured.class)
	public static Result searchEmployee() {

		User user = Secured.getUser(ctx());

		if (user.isAdmin()) {
			LOG.info(String.format("%s User %s entered the page.", LogTag.INFO, user.getUsername()));
			return ok(SearchEmployee.render(Secured.isLoggedIn(ctx()), user, Form.form(SearchFormData.class),
						null, ""));
		} else {
			LOG.error(String.format("%s Unauthorized user tries to attempt this site. User: %s", LogTag.ERROR, user.getUsername()));
			return unauthorized(ErrorPage.render("401 Unauthorized",
													Secured.isLoggedIn(ctx()), user));
		}
	}

	/**
	 * Handles the posting of form data entered by the user.
	 * Author - Ivan Videsvall
	 * @return The Search employee page
	 */
	@Security.Authenticated(Secured.class)
	public static Result postEmployeeNr() {

		User user = Secured.getUser(ctx());

		Form<SearchFormData> formData = searchForm.bindFromRequest();

		if (formData.hasErrors()) {
			LOG.error(String.format("%s User: %s (%s) tries to search after illegal valuse.",
					LogTag.ERROR,
					user.getUsername(),
					user.getFullname()));
			return badRequest(SearchEmployee.render(Secured.isLoggedIn(ctx()), user,
													formData, null, ""));
		} else {

			SearchFormData data = formData.get();

			Absence absence = AbsenceDB.findAbsenceEmployee(data.employeeNr);

			LOG.info(String.format("%s User: %s (%s) search after %s",
					LogTag.SEARCH, user.getUsername(),
					user.getFullname(),
					formData.get().employeeNr));

			if (absence == null) {
				return badRequest(SearchEmployee.render(Secured.isLoggedIn(ctx()), user,
						formData, null, ""));
			} else {
				return ok(SearchEmployee.render(Secured.isLoggedIn(ctx()), user, formData,
						absence, absence.getUser().getFullname()));
			}
		}
	}

	/**
	 * Provides the Login page (only to unauthenticated users).
	 * Author - Ivan Videsvall
	 * @return The Login page
	 */
	public static Result login() {

		return ok(Login.render(Secured.isLoggedIn(ctx()),
								Secured.getUser(ctx()), loginForm));
	}

	/**
	 * Processes a login form submission from an unauthenticated user.
	 * First we bind the HTTP POST data to the Login form.
	 * The binding process will invoke the LoginFormData.validate() method.
	 * If errors are found, re-render the page, displaying the error data.
	 * If errors not found, redirect to appropriate page.
	 *
	 * Author - Ivan Videsvall
	 * @return The index page with the results of validation.
	 */
	public static Result postLogin() {

		// Get the submitted form data from the request object, and run validation.
		loginForm = loginForm.bindFromRequest();

		if (loginForm.hasErrors()) {
			flash("error", "Ogiltiga inloggningsuppgifter.");
			LOG.error(String.format("%s, faild to login.",
					LogTag.ERROR));
			return badRequest(Login.render(Secured.isLoggedIn(ctx()),
											Secured.getUser(ctx()), loginForm));
		} else {
			// username/password OK, so now we set the session variable and only go to authenticated pages.
			session().clear();
			session("username", loginForm.get().username);
			User user = UserDB.getUser(loginForm.get().username);

			LOG.info(String.format("%s User: %s (%s) | Administrator: %s",
					LogTag.LOGIN,
					user.getUsername(),
					user.getFullname(),
					user.isAdmin()));

			return redirect(routes.Registration.index());
		}
	}

	/**
	 * Logs out (only for authenticated users) and returns them to the home page.
	 * Author - Ivan Videsvall
	 * @return A redirect to the home page
	 */
	@Security.Authenticated(Secured.class)
	public static Result logout() throws UnknownHostException {

		LOG.info(String.format("%s User: %s",
				LogTag.LOGOUT,
				Secured.getUser(ctx()).getUsername()));
		session().clear();
		return redirect(routes.Registration.index());
	}

	/**
	 * Returns a 404 Not found error page for non-existing paths.
	 * Author - Ivan Videsvall
	 * @param path the stray path
	 * @return The 404 Not found error page.
	 */
	public static Result missing(String path) {

		User user = Secured.getUser(ctx());

		LOG.error(String.format("%s User: %s | %s",
				LogTag.ERROR,
				user.getUsername(),
				"Tries to reach missing path ->" + path));
		return notFound(ErrorPage.render("404 Not found",
							Secured.isLoggedIn(ctx()), user));
	}
}
