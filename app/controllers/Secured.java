package controllers;

import models.User;
import models.UserDB;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;

/**
 * Implement authorization for this system.
 * getUsername() and onUnauthorized override superclass methods to restrict access.
 *
 * getUser(), isLoggedIn(), and getUser() provide static helper methods so that
 * controllers can know if there is a logged in user.

 * @author Ivan Videsvall on 2014-03-11.
 */
public class Secured extends Security.Authenticator {

	/** A utility object to access object methods from static ones. */
	private static Secured secUtil = new Secured();

	/**
	 * Used by authentication annotation to determine if user is logged in.
	 * @param ctx The context.
	 * @return The username of the logged in user, or null if not logged in.
	 */
	@Override
	public String getUsername(Context ctx) {

		return ctx.session().get("username");
	}

	/**
	 * Instruct authenticator to automatically redirect to login page if unauthorized.
	 * @param ctx The context.
	 * @return The login page.
	 */
	@Override
	public Result onUnauthorized(Context ctx) {

		return redirect(controllers.routes.Application.login());
	}

	/**
	 * True if there is a logged in user, false otherwise.
	 * @param ctx The context.
	 * @return True if user is logged in, otherwise false.
	 */
	public static boolean isLoggedIn(Context ctx) {

		return (secUtil.getUsername(ctx) != null);
	}

	/**
	 * Return the User object of the logged in user, or null if no user is logged in.
	 * @param ctx The context.
	 * @return The User, or null.
	 */
	public static User getUser(Context ctx) {

		return (isLoggedIn(ctx) ? UserDB.getUser(secUtil.getUsername(ctx)) : null);
	}
}
