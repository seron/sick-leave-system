package controllers;

import log.LogTag;
import models.AbsenceDB;
import models.User;
import models.UserDB;
import play.Logger;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Security;
import views.formdata.AbsenceFormData;
import views.formdata.ChildAbsenceFormData;
import views.formdata.MedicalCertFormData;

/**
 * Created by Mikael Ekroth
 * Authentication modified by Ivan Videsvall 140317
 * Controller: Registration
 * Date: 2014-03-11
 * Time: 08:24
 *
 * Description:
 * This controller holds and take care of the action from the registration.
 */
public class Registration extends Controller {

	/** Activating the logging. */
	private static final Logger.ALogger LOG = Logger.of("application");

	/**
	 * GET-method.
	 * Displays the registrations page if the user
	 * is logged in else it redirect to login-page.
	 * @return OK - displays the registration page
	 */
	@Security.Authenticated(Secured.class)
	public static Result index() {
		return ok(views.html.Registration.render(
				Secured.isLoggedIn(ctx()),
				Secured.getUser(ctx()),
				"employee",
				Form.form(AbsenceFormData.class),
				Form.form(ChildAbsenceFormData.class),
				Form.form(MedicalCertFormData.class)
		)
		);
	}

	/**
	 * POST-method
	 * Take care of the form data which comes from
	 * registration of employee absence.
	 * @return OK - displays the registration page
	 */
	@Security.Authenticated(Secured.class)
	public static Result postAbsence() {

	User user = Secured.getUser(ctx());
	//Data from the form
	Form<AbsenceFormData> formData =
			Form.form(AbsenceFormData.class)
					.bindFromRequest();

	LOG.info(String.format("%s User: %s (%s) is sick.",
			LogTag.REGISTER, user.getUsername(),
			user.getFullname()));

	AbsenceFormData data = formData.get();
	AbsenceDB.addAbsence(user.getUsername(), data);

	flash("success", "Sjukanmälan är registrerat.");
	return ok(views.html.Registration.render(
			Secured.isLoggedIn(ctx()),
			Secured.getUser(ctx()),
			"employee",
			formData,
			Form.form(ChildAbsenceFormData.class),
			Form.form(MedicalCertFormData.class)
		)
		);
	}

	/**
	 * POST-method
	 * Take care of the form data which comes from
	 * registration of child absence.
	 * @return OK - displays the registration page
	 */
	@Security.Authenticated(Secured.class)
	public static Result postChildAbsence() {

		Form<ChildAbsenceFormData> formData =
				Form.form(ChildAbsenceFormData.class)
						.bindFromRequest();

		if (formData.hasErrors()) {

			LOG.error(String.format("%s %s (%s) tries to register child sickness with illegal values.",
					LogTag.ERROR,
					Secured.getUser(ctx()).getUsername(),
					Secured.getUser(ctx()).getFullname()));

			return badRequest(views.html.Registration.render(
					Secured.isLoggedIn(ctx()),
					Secured.getUser(ctx()),
					"child",
					Form.form(AbsenceFormData.class),
					formData,
					Form.form(MedicalCertFormData.class)
			)
			);
		} else {
			ChildAbsenceFormData data = formData.get();
			AbsenceDB.addChildAbsence(Secured.getUser(ctx()).getUsername(), data);

			LOG.info(String.format("%s Absence | %s (%s) register child (%s) sickness.",
					LogTag.REGISTER,
					Secured.getUser(ctx()).getUsername(),
					Secured.getUser(ctx()).getFullname(),
					data.registrationNumber));
			flash("success", "Vård av sjukt barn är nu registrerat.");
			return ok(views.html.Registration.render(
					Secured.isLoggedIn(ctx()),
					Secured.getUser(ctx()),
					"child",
					Form.form(AbsenceFormData.class),
					Form.form(ChildAbsenceFormData.class),
					Form.form(MedicalCertFormData.class)
				)
			);
		}
	}

	/**
	 * POST-method
	 * Take care of the form data which comes from
	 * registration of medical certification.
	 * @return OK - displays the registration page
	 */
	@Security.Authenticated(Secured.class)
	public static Result postMedicalCert() {

		Form<MedicalCertFormData> formData =
				Form.form(MedicalCertFormData.class)
						.bindFromRequest();

		if (formData.hasErrors()) {
			LOG.error(String.format("%s %s (%s) tries to register medical certification with illegal values.",
					LogTag.ERROR,
					Secured.getUser(ctx()).getUsername(),
					Secured.getUser(ctx()).getFullname()));

			return badRequest(views.html.Registration.render(
					Secured.isLoggedIn(ctx()),
					Secured.getUser(ctx()),
					"cert",
					Form.form(AbsenceFormData.class),
					Form.form(ChildAbsenceFormData.class),
					formData
			)
			);
		} else {
			MedicalCertFormData data = formData.get();
			AbsenceDB.addMedicalCert(Secured.getUser(ctx()).getUsername(), data);

			LOG.info(String.format("%s Absence | %s (%s) register medical certifaction with START: %s END: %s period.",
					LogTag.REGISTER,
					Secured.getUser(ctx()).getUsername(),
					Secured.getUser(ctx()).getFullname(),
					formData.get().start,
					formData.get().end));
			flash("success", "Läkarintyget är registrerat");
			return ok(views.html.Registration.render(
					Secured.isLoggedIn(ctx()),
					Secured.getUser(ctx()),
					"cert",
					Form.form(AbsenceFormData.class),
					Form.form(ChildAbsenceFormData.class),
					Form.form(MedicalCertFormData.class)));
		}
	}
}
