package log;

import java.net.Inet4Address;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: Mikael Ekroth
 * Date: 2014-03-18
 * Time: 12:20
 * Description:
 */
public enum LogTag {
	ERROR ("** ERROR **"),
	SEARCH ("** SEARCHING **"),
	LOGIN ("** LOGGED IN **"),
	LOGOUT ("** LOGGED OUT **"),
	INFO ("** INFO **"),
	REGISTER ("** REGISTER **");

	private final String tag;

	private LogTag(final String tagname) {
		this.tag = tagname;
	}

	@Override
	public String toString(){
		try {
			return this.tag + " IP: " + Inet4Address.getLocalHost();
		} catch (UnknownHostException e) {
			return null;
		}
	}
}
