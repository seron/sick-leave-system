# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table absence (
  id                        bigint auto_increment not null,
  user_id                   bigint,
  added                     datetime,
  is_sick                   tinyint(1) default 0,
  child_sickness            tinyint(1) default 0,
  child_registration_num    varchar(255),
  medical_cert              tinyint(1) default 0,
  medical_start             datetime,
  medical_end               datetime,
  constraint pk_absence primary key (id))
;

create table user (
  id                        bigint auto_increment not null,
  emplyee_number            varchar(255),
  username                  varchar(255),
  first_name                varchar(255),
  last_name                 varchar(255),
  password                  varchar(255),
  admin                     tinyint(1) default 0,
  constraint pk_user primary key (id))
;

alter table absence add constraint fk_absence_user_1 foreign key (user_id) references user (id) on delete restrict on update restrict;
create index ix_absence_user_1 on absence (user_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table absence;

drop table user;

SET FOREIGN_KEY_CHECKS=1;

